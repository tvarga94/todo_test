<?php

use App\Task;
use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task = new Task();
        $task->user_id = 1;
        $task->title = 'First Task';
        $task->description = 'Long description for the task';
        $task->taskDate = now();
        $task->isCompleted = 0;
        $task->save();


        $task1 = new Task();
        $task1->user_id = 1;
        $task1->title = 'Second Task';
        $task1->description = 'Description for the comleted task';
        $task1->taskDate = now();
        $task1->isCompleted = 1;
        $task1->save();
    }
}
