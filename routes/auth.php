<?php

Route::get('/dashboard', 'TaskController@showTasks');
Route::get('/insertform', 'TaskController@showInsertForm');
Route::post('/inserttask', 'TaskController@insertTask');
Route::get('/deletetask/{id}', 'TaskController@deleteTask');
Route::get('/completed/{id}', 'TaskController@setTaskCompleted');
Route::get('/incompleted/{id}', 'TaskController@setTaskInCompleted');
Route::get('/edit/{id}', 'TaskController@editTask');
Route::post('/editsave/{id}', 'TaskController@editTaskSave');
Route::get('/search', 'TaskController@searchInTasks');
Route::get('/datediff/{id}', 'TaskController@dateDiff');