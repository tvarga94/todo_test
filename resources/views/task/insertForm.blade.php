<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>InsertForm</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
<div class="row">
    <div class="col-md-12 ">

        <div class="user_data">

        </div>
    </div>
</div>
<form action="/inserttask" method="POST" enctype="multipart/form-data">
    <dl class="row">
        <dt class="col-md-6">Task title:</dt>
        <dd class="col-md-6"><input class="form-control" type="text" name="title" /></dd>

        <dt class="col-md-6">Task Description:</dt>
        <dd class="col-md-6"><input class="form-control" type="text" name="description" /></dd>

        <dt class="col-md-6">Task Attachment:</dt>
        <dd class="col-md-6"><input type="file" name="data" id="data"></dd>
    </dl>
    <p><button class="btn btn-success">Save</button></p>
    {{ csrf_field() }}
</form>
</body>
</html>