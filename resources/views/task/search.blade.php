<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Search</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
<table>
    <tr>
    <td>User</td>
    <td>Title</td>
    <td>Description</td>
    </tr>
    <tr>
    @foreach($task as $t)
    <td>{{$t->user->name}}</td>
    <td>{{$t->title}}</td>
    <td>{{$t->description}}</td>
    @endforeach
    </tr>
</table>
<p>
<h2><a href="/dashboard">Back to Dashboard</a></h2>
</body>
</html>