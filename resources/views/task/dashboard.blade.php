<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
    <a class="dropdown-item" href="{{ route('logout') }}"
       onclick="event.preventDefault();
       document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>
<?php
  $taskController = new \App\Http\Controllers\TaskController();
?>
<h2>Search in tasks</h2>
<form action="/search" method="GET">
<input type="text" name="text" /><button>Search</button>
</form>
<p>
<a id='button' href="{{ url('insertform') }}">Register new task</a>
<table border="1">
    <tr><td>User</td>
        <td>Title</td>
        <td>Description</td>
        <td>isCompleted</td>
        <td>TaskDate</td>
        <td>Date Since action</td>
        <td>Task Status</td>
        <td>Hide and Show</td>
        <td>Edit Task</td>
        <th>Delete Task</th>
    </tr>
    @foreach($tasks as $task)
        <tr>
            <td>{{$task->user->name}}</td>
            <td>{{$task->title}}</td>
            <td>{{$task->description}}</td>
            <td>{{$task->isCompleted}}</td>
            <td>{{$task->taskDate}}</td>
            <td>{{$taskController->dateDiff($task->id)}}</td>
            @if($task->isCompleted == 0)
                <td colspan="2"><a href="/completed/{{ $task->id }}">Complete Task</a></td>
            @else
                <td class="done"><a href="/incompleted/{{ $task->id }}">Incomplete Task</a></td>
                <td><button id="show">Show</button><button id="hide">Hide</button></td>
            @endif
            <td><a href="/edit/{{$task->id}}">Edit</a></td>
            <td><a href="/deletetask/{{ $task->id }}">Delete task</a></td>
        </tr>
    @endforeach
</table>
</body>
</html>