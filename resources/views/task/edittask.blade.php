<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Task</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
<form action="/editsave/{{$task->id}}" method="POST">
    <table>
        <tr>
            <td>Title</td>
            <td>Description</td>
        </tr>
        <tr>
            <td><input type="text" name="title" value="{{$task->title}}" /></td>
            <td><input type="text" name="description" value="{{$task->description}}" /></td>
        </tr>
        <tr>
            <td colspan="2"><button class="btn btn-success">Save</button></td>
            {{ csrf_field() }}
        </tr>
    </table>
</form>
</body>
</html>