<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'task';

    protected $fillable = [
        'user_id', 'title', 'description', 'taskDate', 'isCompleted'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
