<?php

namespace App\Http\Controllers;

use App\Task;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    public function showTasks(){
        $user_id = Auth::id();
        $tasks = Task::where('user_id', $user_id)->orderBy('isCompleted', 'ASC')->orderBy('taskDate', 'DESC')->get();

        return view('task/dashboard', compact('tasks'));
    }

    public function showInsertForm(){
        return view('task/insertForm');
    }

    public function insertTask(Request $request){
        $user_id = Auth::id();

        $data = $request->data;
        $dbUrl = "http://".$request->getHttpHost()."../storage/documents/";
        $path = "../storage/documents/";

        $documentumRules = array(
            'data' => 'required|mimes:jpg,xml,pdf|max:5000'
        );

        $image = array('data' => $data);
        $imageValidator = Validator::make($image, $documentumRules);

        if($imageValidator->fails()) {
            return 'not supported format';
        }

        $file = $data;
        $name = explode($file->getClientOriginalExtension(), $file->getClientOriginalName())[0];
        $ext = strtolower($file->getClientOriginalExtension());
        $name = time() . "-" . str_slug(hash('sha256', $name),'-'). '.' . $ext;
        $file->move($path, $name);
        Task::create(['user_id' => $user_id, 'title' => $request->title, 'description' => $request->description, 'taskDate' => now(), 'attachment' => $name]);

        return $this->showTasks();
    }

    public function deleteTask($id){
        Task::find($id)->delete();

        return $this->showTasks();
    }

    public function setTaskCompleted($id){
        $completed = Task::find($id);
        $completed->isCompleted = 1;
        $completed->taskDate = now();
        $completed->save();

        return $this->showTasks();
    }

    public function setTaskInCompleted($id){
        $completed = Task::find($id);
        $completed->isCompleted = 0;
        $completed->taskDate = now();
        $completed->save();

        return $this->showTasks();
    }

    public function editTask($id){
        $task = Task::find($id);

        return view('task/edittask', compact ('task'));

    }

    public function editTaskSave(Request $request, $id){
        $task = Task::find($id);
        $task->title = $request->title;
        $task->description = $request->description;
        $task->save();

        return $this->showTasks();
    }

    public function searchInTasks(Request $request){
        $search = $request->text;

        $task = Task::where('title', 'LIKE', '%' . trim($search) . '%')->orWhere('description', 'LIKE', '%' . trim($search). '%')->get();

        return view('task/search', compact('task'));
    }

    public function dateDiff($id){
        $date = Task::find($id);
        $db_date = $date->taskDate;
        $current = now();
        $diff = abs(strtotime($db_date) - strtotime($current));

        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        $hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
        $minutes  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
        $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

        return $years. " years, ". $months. " months, ". $days. " days, ". $hours. " hours, ". $minutes. " minutes, ". $seconds. " seconds" ;
    }

}
